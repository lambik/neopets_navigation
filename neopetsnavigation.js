function updateSub(selection, target) {
	target.options.length = 0;

	switch (selection) {
		case 'The World of Neopia':
			target.options[0] = new Option('Overview', 'http://www.neopets.com/explore.phtml');
			break;

		case 'Neopia Central':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/objects.phtml');
				options[1] = new Option('Auction House', 'http://www.neopets.com/auctions.phtml');
				options[2] = new Option('Neopian Bank', 'http://www.neopets.com/bank.phtml');
				options[3] = new Option('Food Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=1');
				options[4] = new Option('Book Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=7');
				options[5] = new Option('Money Tree', 'http://www.neopets.com/donations.phtml');
				options[6] = new Option('Rainbow Pool', 'http://www.neopets.com/rainbowpool.phtml');
				options[7] = new Option('Pharmacy', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=13');
				options[8] = new Option('Magic Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=2');
				options[9] = new Option('Petpet Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=25');
				options[10] = new Option('Neolodge', 'http://www.neopets.com/neolodge.phtml');
				options[11] = new Option('Post Office', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=58');
				options[12] = new Option('Shop of Offers', 'http://www.neopets.com/shop_of_offers.phtml');
				options[13] = new Option('Zazzle T-shirts', 'http://www.neopets.com/process_click.phtml?type_id=12&item_id=5987');
				
				options[14] = new Option('99dogs Store', 'http://www.neopets.com/sponsors/sponsors_99dogs.phtml');
				options[15] = new Option('Thinkway Toys', 'http://www.neopets.com/process_click.phtml?item_id=2046');
				options[16] = new Option('Disney Theatre', 'http://www.neopets.com/process_click.phtml?item_id=1490');
			}
			break;
			
		case 'Neopian Bazaar':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/market_bazaar.phtml');
				options[1] = new Option('Petpet supplies', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=69');
				options[2] = new Option('Grooming Parlour', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=5');
				options[3] = new Option('Health Food', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=16');
				options[4] = new Option('Toy Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=3');
				options[5] = new Option('Hubert\'s Hotdogs', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=46');
				options[6] = new Option('Chocolate Factory', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=14');
				options[7] = new Option('Defence Magic', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=10');
				options[8] = new Option('Usuki Land', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=48');
				options[9] = new Option('Battle Magic', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=9');
				options[10] = new Option('Fresh Smoothies', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=18');
				options[11] = new Option('Uni\'s Clothing', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=4');
				options[12] = new Option('Gifts Galore', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=17');
				options[13] = new Option('Gardening Supplies', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=12');
				options[14] = new Option('The Bakery', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=15');
				options[15] = new Option('Fine Furniture', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=41');
				options[16] = new Option('Collectable Card Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=8');
				options[17] = new Option('Wizards Shop', 'http://www.neopets.com/process_click.phtml?type_id=12&item_id=2894');
				options[18] = new Option('Survey Shack', 'http://www.neopets.com/sponsors/surveyshack/index.phtml');
				options[19] = new Option('Movie Mountain', 'http://www.neopets.com/sponsors/moviemountain/index.phtml');
			}
			break;
			
		case 'Neopian Marketplace':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/market_map.phtml');
				options[1] = new Option('Shop Wizard', 'http://www.neopets.com/market.phtml?type=wizard');
				options[2] = new Option('Soup Kitchen', 'http://www.neopets.com/soupkitchen.phtml');
			}
			break;
			
		case 'Neopian Plaza':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/market_plaza.phtml');
				options[1] = new Option('Plushie shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=98');
				options[2] = new Option('Welcome center', 'http://www.neopets.com/petcentral.phtml');
				options[3] = new Option('Alien Vending Machine', 'http://www.neopets.com/vending.phtml');
				options[4] = new Option('Pizzaroo', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=47');
				options[5] = new Option('Music Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=84');
				options[6] = new Option('Neopian pound', 'http://www.neopets.com/pound.phtml');
				options[7] = new Option('School supplies', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=53');
				options[8] = new Option('Hospital', 'http://www.neopets.com/hospital.phtml');
				options[9] = new Option('Kadoatery', 'http://www.neopets.com/games/kadoatery/index.phtml');
				options[10] = new Option('Wishing Well', 'http://www.neopets.com/wishing.phtml');
				options[11] = new Option('Defenders HQ', 'http://www.neopets.com/games/defenders_choose.phtml');
			}
			break;
			
		case 'Art Centre':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/art/index.phtml');
				options[1] = new Option('Neopian Times', 'http://www.neopets.com/newnt/index.phtml');
				options[2] = new Option('Story Telling', 'http://www.neopets.com/art/storytell.phtml');
				options[3] = new Option('Coin Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=68');
				options[4] = new Option('Coffee Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=34');
				options[5] = new Option('Poetry Contest', 'http://www.neopets.com/contributions_poems.phtml');
				options[6] = new Option('How To Draw', 'http://www.neopets.com/art/drawing.phtml');
				options[7] = new Option('Art Gallery', 'http://www.neopets.com/art/gallery.phtml');
			}
			break;
			
		case 'Haunted Woods':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/halloween/index.phtml');
				options[1] = new Option('Brain Tree', 'http://www.neopets.com/halloween/braintree.phtml');
				options[2] = new Option('Stone Battledome', 'http://www.neopets.com/halloween/scaryimages/stonedome.phtml');
				options[3] = new Option('Fetch', 'http://www.neopets.com/games/maze/maze.phtml');
				options[4] = new Option('Spooky Paintbrushes', 'http://www.neopets.com/halloween/costumes.phtml');
				options[5] = new Option('Esophagor', 'http://www.neopets.com/halloween/esophagor.phtml');
				options[6] = new Option('Colouring Pages', 'http://www.neopets.com/halloween/colouring_page.phtml');
				options[7] = new Option('Castle of Eliv Thade', 'http://www.neopets.com/games/elivthade.phtml');
				options[8] = new Option('Neopet Masks', 'http://www.neopets.com/halloween/spet_masks.phtml');
				options[9] = new Option('Spooky Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=31');
				options[10] = new Option('Magax: Destroyer', 'http://www.neopets.com/games/magax.phtml');
				options[11] = new Option('Game Graveyard', 'http://www.neopets.com/halloween/gamegraveyard.phtml');
				options[12] = new Option('Edna\'s Tower', 'http://www.neopets.com/halloween/witchtower.phtml');
				options[13] = new Option('Haunted House', 'http://www.neopets.com/halloween/haunted_house.phtml');
				
				options[14] = new Option('Gypsy Camp', 'http://www.neopets.com/halloween/hwp/gypsy_camp.phtml');
			}
			break;
			
		case 'Deserted Fairground':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/halloween/index_fair.phtml');
				options[1] = new Option('Wheel of Misfortune', 'http://www.neopets.com/halloween/wheel/index.phtml');
				options[2] = new Option('Spooky Food', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=30');
				options[3] = new Option('Test Your Strength', 'http://www.neopets.com/halloween/strtest/index.phtml');
				options[4] = new Option('Carnival of Terror', 'http://www.neopets.com/games/carnival.phtml');
				options[5] = new Option('Spooky Furniture', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=60');
				options[6] = new Option('Scratch Cards', 'http://www.neopets.com/halloween/scratch.phtml');
				options[7] = new Option('Ghost Neopets', 'http://www.neopets.com/halloween/ghostpets.phtml');
				options[8] = new Option('Haunted Weaponry', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=59');
				options[9] = new Option('Coconut Shy', 'http://www.neopets.com/halloween/coconutshy.phtml');
				options[10] = new Option('Bagatelle', 'http://www.neopets.com/halloween/bagatelle.phtml');
				options[11] = new Option('Cork Gun', 'http://www.neopets.com/halloween/corkgun.phtml');
			}
			break;
		
		case 'Lost Desert':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/desert/index.phtml');
				options[2] = new Option('Sutek\'s Tomb', 'http://www.neopets.com/games/sutekstomb.phtml');
				options[3] = new Option('Pyramids Game', 'http://www.neopets.com/games/pyramids/index.phtml');
				options[4] = new Option('Coltzan\'s Shrine', 'http://www.neopets.com/desert/shrine.phtml');
				options[5] = new Option('Calculator', 'http://www.neopets.com/desert/calculator.phtml');
				options[6] = new Option('Swarm!', 'http://www.neopets.com/games/swarm.phtml');
				options[7] = new Option('Tug-o-war', 'http://www.neopets.com/games/tugowar.phtml');

				options[8] = new Option('Brucey\'s War Tent', 'http://www.neopets.com/desert/wartent.phtml');
				
//				options[9] = new Option('Temple of 1000 tombs', 'http://www.neopets.com/desert/ldp/temple.phtml');
//				options[10] = new Option('Fortune Teller', 'http://www.neopets.com/desert/ldp/fortune_teller.phtml');
//				options[11] = new Option('Mysterious tablet', 'http://www.neopets.com/desert/ldp/tablet.phtml');
//				options[12] = new Option('Scroll Repository', 'http://www.neopets.com/desert/ldp/repo_gate.phtml');
//				options[13] = new Option('The Gift Shop', 'http://www.neopets.com/desert/ldp/gift_shop.phtml');
			}
			break;
			//http://www.neopets.com/desert/ldp/solution.phtml
		
		case 'Sakhmet':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/desert/sakhmet.phtml');
				options[1] = new Option('Petpet Stall', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=50');
				options[2] = new Option('Food Stall', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=49');
				options[3] = new Option('Colouring Pages', 'http://www.neopets.com/desert/colouringpages.phtml');
				options[4] = new Option('Scarab21', 'http://www.neopets.com/games/scarab21/index.phtml');
				options[5] = new Option('Sakhmet Solitaire', 'http://www.neopets.com/games/sakhmet_solitaire/index.phtml');
				options[6] = new Option('Osiris Pottery', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=55');
				options[7] = new Option('Lost Desert Medicine', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=85');
				options[8] = new Option('Geos', 'http://www.neopets.com/games/geos/geos.phtml');
				options[9] = new Option('Paintbrush Stall', 'http://www.neopets.com/desert/paintbrushes.phtml');
				options[10] = new Option('Sutek\'s Scrolls', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=51');
				options[11] = new Option('Scratch Cards', 'http://www.neopets.com/desert/sc/kiosk.phtml');
				options[12] = new Option('Fruit Machine', 'http://www.neopets.com/desert/fruitmachine.phtml');
				options[13] = new Option('Battle Supplies', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=54');
				options[14] = new Option('Osiris Pottery', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=55');
				options[15] = new Option('Sakhmet Palace', 'http://www.neopets.com/desert/usurper.phtml');
			}
			break;
		
		case 'Qasala':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/desert/qasala.phtml');
				options[1] = new Option('Words of Antiquity', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=92');
				options[2] = new Option('Desert Arms', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=91');
				options[3] = new Option('Qasalan Delights', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=90');
			}
			break;
		
		case 'Faerieland':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/faerieland/index.phtml');
				options[1] = new Option('Faerie Cloud Racers', 'http://www.neopets.com/games/cloudracers.phtml');
				options[2] = new Option('Poogle Races', 'http://www.neopets.com/faerieland/poogleracing.phtml');
				options[3] = new Option('Rainbow Fountain', 'http://www.neopets.com/faerieland/rainbowfountain.phtml');
				options[4] = new Option('Jhudora\'s Cloud', 'http://www.neopets.com/faerieland/darkfaerie.phtml');
				options[5] = new Option('Healing Springs', 'http://www.neopets.com/faerieland/springs.phtml');
				options[6] = new Option('Faerie Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=40');
				options[7] = new Option('Wheel of Excitement', 'http://www.neopets.com/faerieland/wheel.phtml');
				options[8] = new Option('Blue Grundo Plushie', 'http://petpages.neopets.com/faerieland/tdmbgpop.phtml');
			}
			break;
			
		case 'Faerie City':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/faerieland/faeriecity.phtml');
				options[1] = new Option('Maths Nightmare', 'http://www.neopets.com/games/mathsnightmare.phtml');
				options[2] = new Option('Faerie Paintbrushes', 'http://www.neopets.com/faerieland/brushes.phtml');
				options[3] = new Option('Colouring Pages', 'http://www.neopets.com/faerieland/faerie_colour.phtml');
				options[4] = new Option('Faerie Furniture', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=75');
				options[5] = new Option('Employment Agency', 'http://www.neopets.com/faerieland/employ/employment.phtml');
				options[6] = new Option('Faerie Bookshop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=38');
				options[7] = new Option('Faerie Crossword', 'http://www.neopets.com/games/crossword/index.phtml');
				options[8] = new Option('Faerie Quests', 'http://www.neopets.com/quests.phtml');
				options[9] = new Option('Faerie Foods', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=39');
				options[10] = new Option('Hidden Tower', 'http://www.neopets.com/faerieland/hiddentower938.phtml');
				options[11] = new Option('Faerie Weapons', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=93');
			}
			break;
			
		case 'Altador':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/altador/index.phtml');
				options[1] = new Option('Altadorian Archives', 'http://www.neopets.com/altador/archives.phtml');
				options[2] = new Option('Exquisite Ambrosia', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=95');
				options[3] = new Option('Illustrious Armoury', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=94');
				options[4] = new Option('Magical Marvels', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=96');
				options[5] = new Option('Legendary Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=97');
				options[6] = new Option('Hall of Heroes', 'http://www.neopets.com/altador/hallofheroes.phtml');
				options[7] = new Option('Restive Tomb', 'http://www.neopets.com/altador/tomb.phtml');
				options[8] = new Option('Rock Quarry', 'http://www.neopets.com/altador/quarry.phtml');
			}
			break;

		case 'Krawk Island':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/pirates/index.phtml');
				options[1] = new Option('Deck Ball', 'http://www.neopets.com/games/deckball.phtml');
				options[2] = new Option('Krawk Cup', 'http://www.neopets.com/pirates/championship.phtml');
				options[3] = new Option('Smuggler\'s Cove', 'http://www.neopets.com/pirates/smugglerscove.phtml');
				options[4] = new Option('Dubloon O Matic', 'http://www.neopets.com/pirates/dubloonomatic.phtml');
				options[5] = new Option('Bilge Dice', 'http://www.neopets.com/pirates/bilge.phtml');
				options[6] = new Option('Food Club', 'http://www.neopets.com/pirates/foodclub.phtml');
				options[7] = new Option('Little Nippers Petpets', 'http://www.neopets.com/pirates/piratepets.phtml');
				options[8] = new Option('Krawk Fashion', 'http://www.neopets.com/pirates/fashion.phtml');
				options[9] = new Option('Armada', 'http://www.neopets.com/games/armada/index.phtml');
				options[10] = new Option('Buried Treasure', 'http://www.neopets.com/pirates/buriedtreasure/index.phtml');
				options[11] = new Option('Academy', 'http://www.neopets.com/pirates/academy.phtml');
				options[12] = new Option('Deck Swabber', 'http://www.neopets.com/games/deckswabber.phtml');
				options[13] = new Option('Krawps', 'http://www.neopets.com/pirates/krawp.phtml');
				options[14] = new Option('Golden Dubloon', 'http://www.neopets.com/pirates/restaurant.phtml');
				options[15] = new Option('Dubloon Disaster', 'http://www.neopets.com/games/dubloondisaster.phtml');
				options[16] = new Option('Fungus Cave', 'http://www.neopets.com/pirates/funguscave.phtml');
			}
			break;
			
		case 'New Maraqua':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/water/');
				options[1] = new Option('Ruins', 'http://www.neopets.com/water/index_ruins.phtml');
				options[2] = new Option('Collectable Sea Shells', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=86');
				options[3] = new Option('Battledome items', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=87');
				options[4] = new Option('Maraquan NeoHomes', 'http://www.neopets.com/neohome.phtml?type=view');
				options[5] = new Option('Maraquan Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=88');
				options[6] = new Option('Kelp', 'http://www.neopets.com/water/restaurant.phtml');
			}
			break;
				
		case 'Ruins Of Maraqua':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/water/index_ruins.phtml');
				options[1] = new Option('Bubbling Pit', 'http://www.neopets.com/water/bubblingpit.phtml');
				options[2] = new Option('Colouring Pages', 'http://www.neopets.com/water/maraqua_colour.phtml');
				options[3] = new Option('Mysterious Statue', 'http://www.neopets.com/water/statue.phtml');
				options[4] = new Option('Underwater Fishing', 'http://www.neopets.com/water/fishing.phtml');
			}
			break;
		
		case 'Mystery Island':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/island/index.phtml');
				options[1] = new Option('Island Mystic', 'http://www.neopets.com/island/mystichut.phtml');
				options[2] = new Option('Cooking Pot', 'http://www.neopets.com/island/cookingpot.phtml');
				options[3] = new Option('Rock Pool', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=27');
				options[4] = new Option('Gadgads Game', 'http://www.neopets.com/games/gadgadsgame.phtml');
				options[5] = new Option('Trading Post', 'http://www.neopets.com/island/tradingpost.phtml');
//				options[6] = new Option('Techo Mountain', 'http://www.neopets.com/island/techomountain.phtml');
				options[6] = new Option('Techo Volcano Mountain', 'http://www.neopets.com/island/volcano_codestone.phtml');
				options[7] = new Option('Training School', 'http://www.neopets.com/island/training.phtml');
				options[8] = new Option('Beach', 'http://www.neopets.com/island/beach.phtml');
				options[9] = new Option('Island Arena', 'http://www.neopets.com/island/abandonedarena.phtml');
				options[10] = new Option('Island Market', 'http://www.neopets.com/island/island_market.phtml');
				options[11] = new Option('Kitchen Quests', 'http://www.neopets.com/island/kitchen.phtml');
				options[12] = new Option('Haiku Generator', 'http://www.neopets.com/island/haiku/haiku.phtml');
				options[13] = new Option('Tiki Tours', 'http://www.neopets.com/island/tikitours.phtml');
				options[14] = new Option('Tropical Foods', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=20');
				options[15] = new Option('Tombola', 'http://www.neopets.com/island/tombola.phtml');
				options[16] = new Option('Beach Volleyball', 'http://www.neopets.com/games/volleyball.phtml');
				options[17] = new Option('Tiki Tack', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=21');
				options[18] = new Option('Harbour', 'http://www.neopets.com/island/return.phtml');
				options[19] = new Option('Parrot', 'http://www.neopets.com/island/parrot.phtml');
				options[20] = new Option('Secret Ninja Training School', 'http://www.neopets.com/island/fight_training.phtml');
				options[21] = new Option('Lost City of Geraptiku', 'http://www.neopets.com/worlds/index_geraptiku.phtml');
			}
			break;
			
		case 'Space Station':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/space/index.phtml');
				options[1] = new Option('Space Armour', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=24');
				options[2] = new Option('Space Battledome', 'http://www.neopets.com/battledome/battledome.phtml?battle_location=4');
				options[3] = new Option('Grundo Warehouse', 'http://www.neopets.com/space/warehouse/prizecodes.phtml');
				options[4] = new Option('Splat a Sloth', 'http://www.neopets.com/games/splatasloth.phtml');
				options[5] = new Option('Space Weapons', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=23');
				options[6] = new Option('Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=26');
				options[7] = new Option('Evil Fuzzles', 'http://www.neopets.com/games/fuzzles.phtml');
				options[8] = new Option('Space Adoption Agency', 'http://www.neopets.com/space/spaceadoption.phtml');
				options[9] = new Option('Spell-Or-Starve', 'http://www.neopets.com/games/spellorstarve.phtml');
				options[10] = new Option('Gormball', 'http://www.neopets.com/space/gormball.phtml');
				options[11] = new Option('Grundos', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=22');
				options[12] = new Option('Marketplace', 'http://www.neopets.com/space/space_market.phtml');
				options[13] = new Option('Strange Lever', 'http://www.neopets.com/space/strangelever.phtml');
			}
			break;
			
		case 'Tyrannia':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/prehistoric/index.phtml');
				options[1] = new Option('Keno', 'http://www.neopets.com/prehistoric/keno.phtml');
				options[2] = new Option('Tyrannian Food', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=42');
				options[3] = new Option('Furniture', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=43');
				options[4] = new Option('Village', 'http://www.neopets.com/prehistoric/village.phtml');
				options[5] = new Option('Tyranu Evavu', 'http://www.neopets.com/games/tyranuevavu.phtml');
				options[6] = new Option('Wheel of Mediocrity', 'http://www.neopets.com/prehistoric/mediocrity.phtml');
				options[7] = new Option('Minigolf', 'http://www.neopets.com/games/minigolf.phtml');
				options[8] = new Option('Cave Painting', 'http://www.neopets.com/prehistoric/painting.phtml');
				options[9] = new Option('Chia Bomber', 'http://www.neopets.com/games/chiabomber.phtml');
				options[10] = new Option('Volcano Run', 'http://www.neopets.com/games/volcanorun.phtml');
				options[11] = new Option('Tyrannian Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=44');
			}
			break;
			
		case 'Tyrannian Plateau':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/prehistoric/plateau.phtml');
				options[1] = new Option('Ticket Booth', 'http://www.neopets.com/prehistoric/ticketbooth.phtml');
				options[2] = new Option('Concert Hall', 'http://www.neopets.com/prehistoric/concerthall.phtml');
				options[3] = new Option('Kacheekers', 'http://www.neopets.com/games/kacheekers/kacheekers.phtml');
				options[4] = new Option('Lair of the Beast', 'http://www.neopets.com/prehistoric/thebeast.phtml');
				options[5] = new Option('Pterattack', 'http://www.neopets.com/games/pterattack.phtml');
				options[6] = new Option('Tyrannian Arena', 'http://www.neopets.com/battledome/battledome.phtml?battle_location=6');
				options[7] = new Option('Giant Omelette', 'http://www.neopets.com/prehistoric/omelette.phtml');
				options[8] = new Option('Tyrannian Weaponry', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=45');
				options[9] = new Option('Chomby Fungus Balls', 'http://www.neopets.com/games/chomby.phtml');
				options[10] = new Option('Wheel of Monotony', 'http://www.neopets.com/prehistoric/monotony/monotony.phtml');
				options[11] = new Option('Destruct-o-Match', 'http://www.neopets.com/games/destructomatch.phtml');
				options[12] = new Option('GoGoGo!', 'http://www.neopets.com/prehistoric/gogogo/index.phtml');
				options[13] = new Option('Town Hall', 'http://www.neopets.com/prehistoric/townhall.phtml');
				options[14] = new Option('DestructoMatch 2', 'http://www.neopets.com/games/destructomatch2.phtml');
			}
			break;
			
		case 'Meridell':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/medieval/index.phtml');
				options[1] = new Option('Turmaculus', 'http://www.neopets.com/medieval/turmaculus.phtml');
				options[2] = new Option('Cheese Roller', 'http://www.neopets.com/medieval/cheeseroller.phtml');
				options[3] = new Option('Illusen\'s Glade', 'http://www.neopets.com/medieval/earthfaerie.phtml');
				options[4] = new Option('Food Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=56');
				options[5] = new Option('Ultimate Bullseye', 'http://www.neopets.com/games/bullseye.phtml');
				options[6] = new Option('Round Table Poker', 'http://www.neopets.com/games/draw_poker/round_table_poker.phtml');
				options[7] = new Option('Turdle Racing', 'http://www.neopets.com/medieval/turdleracing.phtml');
				options[8] = new Option('Petpets Galore', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=57');
				options[9] = new Option('Kiss the Mortog', 'http://www.neopets.com/medieval/kissthemortog.phtml');
				options[10] = new Option('Shapeshifter', 'http://www.neopets.com/medieval/shapeshifter_index.phtml');
				options[11] = new Option('Draik Nest', 'http://www.neopets.com/medieval/draiknest.phtml');
				options[12] = new Option('Mysterious hole', 'http://www.neopets.com/medieval/symolhole.phtml');
			}
			break;
			
		case 'Meri Acres':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/medieval/index_farm.phtml');
				options[1] = new Option('Pick Your Own', 'http://www.neopets.com/medieval/pickyourown_index.phtml');
				options[2] = new Option('Potato Counter', 'http://www.neopets.com/medieval/potatocounter.phtml');
				options[3] = new Option('EXTREME Potato Counter', 'http://www.neopets.com/games/epc.phtml');
				options[4] = new Option('Rubbish Dump', 'http://www.neopets.com/medieval/rubbishdump.phtml');
				options[5] = new Option('Guess the Weight', 'http://www.neopets.com/medieval/guessmarrow.phtml');
				options[6] = new Option('Attack Of The Slorgs', 'http://www.neopets.com/games/slorgattack.phtml');
			}
			break;
			
		case 'Brightvale':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/medieval/brightvale.phtml');
				options[1] = new Option('Wheel Of Knowledge', 'http://www.neopets.com/medieval/knowledge.phtml');
				options[2] = new Option('Royal Potionery', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=83');
				options[3] = new Option('Brightvale Glaziers', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=79');
				options[4] = new Option('Scrollery', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=78');
				options[5] = new Option('Brightvale Armoury', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=80');
				options[6] = new Option('Brightvale Books', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=77');
				options[7] = new Option('Fruits of Brightvale', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=81');
				options[8] = new Option('Brightvale Motery', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=82');
				options[9] = new Option('Wise King', 'http://www.neopets.com/medieval/wiseking.phtml');
			}
			break;

		case 'Meridell Castle':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/medieval/index_castle.phtml');
				options[1] = new Option('Double or Nothing', 'http://www.neopets.com/medieval/doubleornothing.phtml');
				options[2] = new Option('Grumpy Old King', 'http://www.neopets.com/medieval/grumpyking.phtml');
				options[3] = new Option('Kayla\'s Potion Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=73');
				options[4] = new Option('Meriball', 'http://www.neopets.com/games/meriball.phtml');
				options[5] = new Option('Invasion of Meridell', 'http://www.neopets.com/games/iom/index.phtml');
				options[6] = new Option('Escape from Meridell', 'http://www.neopets.com/games/draikcastle.phtml');
			}
			break;
			
		case 'Darigan Citadel':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/medieval/index_evil.phtml');
				options[1] = new Option('Darigan\'s Chambers', 'http://www.neopets.com/medieval/dariganschambers.phtml');
				options[2] = new Option('Cell Block', 'http://www.neopets.com/games/cellblock/cellblock.phtml');
				options[3] = new Option('Petpet Arena', 'http://www.neopets.com/games/petpet_battle/index.phtml');
				options[4] = new Option('Darigan Toys', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=74');
				options[5] = new Option('Minions of Darigan', 'http://www.neopets.com/medieval/minions.phtml');
				options[6] = new Option('Colouring Pages', 'http://www.neopets.com/medieval/colouring.phtml');
			}
			break;
			
		case 'Happy Valley':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/winter/index.phtml');
				options[1] = new Option('Wintery Petpets', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=61');
				options[2] = new Option('Rink Runner', 'http://www.neopets.com/games/rinkrunner.phtml');
				options[3] = new Option('Grundo Snow Throw', 'http://www.neopets.com/games/snowthrow.phtml');
				options[4] = new Option('Snow Wars', 'http://www.neopets.com/games/snowwars.phtml');
				options[5] = new Option('Advent Calendar', 'http://www.neopets.com/winter/adventcalendar.phtml');
				options[6] = new Option('Gift Tags', 'http://www.neopets.com/winter/present_tags.phtml');
				options[7] = new Option('Slushie Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=35');
				options[8] = new Option('Merry Outfits', 'http://www.neopets.com/winter/merryoutfits.phtml');
				options[9] = new Option('Ice Cream', 'http://www.neopets.com/winter/icecream.phtml');
			}
			break;
			
		case 'Ice Caves':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/winter/icecaves.phtml');
				options[1] = new Option('Ice Arena', 'http://www.neopets.com/winter/icearena.phtml');
				options[2] = new Option('Scratch Card Kiosk', 'http://www.neopets.com/winter/kiosk.phtml');
				options[3] = new Option('Snowager', 'http://www.neopets.com/winter/snowager.phtml');
				options[4] = new Option('Neggery', 'http://www.neopets.com/winter/neggery.phtml');
				options[5] = new Option('Ice Crystal Emporium', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=36');
			}
			break;
			
		case 'Terror Mountain':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/winter/terrormountain.phtml');
				options[1] = new Option('Snow Food', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=37');
				options[2] = new Option('Garage Sale', 'http://www.neopets.com/winter/igloo.phtml');
				options[3] = new Option('Garage Sale Game', 'http://www.neopets.com/games/igloogarage.phtml');
				options[4] = new Option('Snow Quests', 'http://www.neopets.com/winter/snowfaerie.phtml');
				options[5] = new Option('Shop of Mystery', 'http://www.neopets.com/winter/shopofmystery.phtml');
				options[6] = new Option('Cliff Hanger', 'http://www.neopets.com/games/cliffhanger.phtml');
				options[7] = new Option('Colouring Pages', 'http://www.neopets.com/winter/colouring.phtml');
				options[8] = new Option('Toy Repair Shop', 'http://www.neopets.com/winter/brokentoys.phtml');
			}
			break;
			
		case 'Kreludor':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/moon/index.phtml');
				options[1] = new Option('Mining Corp.', 'http://www.neopets.com/moon/mining.phtml');
				options[2] = new Option('Booktastic Books', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=70');
				options[3] = new Option('Cafe Kreludor', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=72');
				options[4] = new Option('Neocola Machine', 'http://www.neopets.com/moon/neocola.phtml');
				options[5] = new Option('Kreludan Homes', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=71');
				options[6] = new Option('Colouring Pages', 'http://www.neopets.com/moon/colour.phtml');
			}
			break;
			
		case 'Lost City of Geraptiku':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/worlds/index_geraptiku.phtml');
				options[1] = new Option('Deserted Tomb', 'http://www.neopets.com/worlds/geraptiku/tomb.phtml');
				options[2] = new Option('Petpet Shop', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=89');
				options[3] = new Option('Colouring pages', 'http://www.neopets.com/worlds/geraptiku/colouring.phtml');
			}
			break;
			
		case 'Roo Island':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/worlds/index_roo.phtml');
				options[1] = new Option('Souvenirs', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=76');
				options[2] = new Option('Roo Island Properties', 'http://www.neopets.com/neohome.phtml?type=view');
				options[3] = new Option('Deadly Dice', 'http://www.neopets.com/worlds/deadlydice.phtml');
				options[4] = new Option('Merry Go Round', 'http://www.neopets.com/worlds/roo/merrygoround.phtml');
				options[5] = new Option('Dice a Roo', 'http://www.neopets.com/games/dicearoo.phtml');
				options[6] = new Option('Colouring Pages', 'http://www.neopets.com/worlds/roo/colouring_page.phtml');
			}
			break;
			
		case 'Kiko Lake':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/worlds/index_kikolake.phtml');
				options[1] = new Option('Kiko Lake Carpentry', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=67');
				options[2] = new Option('Kiko Lake Treats', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=66');
				options[3] = new Option('Glass Bottom Boat Tours', 'http://www.neopets.com/worlds/kiko/glass_boat.phtml');
				options[4] = new Option('Colouring Pages', 'http://www.neopets.com/worlds/kiko/colouring_page.phtml');
			}
			break;

		case 'Jelly World':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/jelly/');
				options[1] = new Option('Green Jelly', 'http://www.neopets.com/jelly/greenjelly.phtml');
				options[2] = new Option('Giant Jelly', 'http://www.neopets.com/jelly/jelly.phtml');
				options[3] = new Option('Blobs of Doom', 'http://www.neopets.com/games/jellyblobs.phtml');
				options[4] = new Option('Jelly Foods', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=62');
				options[5] = new Option('Colouring Pages', 'http://www.neopets.com/jelly/colouring_page.phtml');
			}
			break;
			
		case 'Movie Mountain':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/sponsors/moviemountain/index.phtml');
				options[1] = new Option('Souvenirs', 'http://www.neopets.com/sponsors/moviemountain/objects_mm.phtml');
				options[2] = new Option('Over the hedge', 'http://www.neopets.com/games/overthehedge/game1.phtml');
				options[3] = new Option('Snackbar', 'http://www.neopets.com/sponsors/moviemountain/snackbar.phtml');
				options[4] = new Option('Movie pics', 'http://www.neopets.com/sponsors/moviemountain/movie_pics.phtml');
			}
			break;

		case 'Shenkuu':
			with (target) {
				options[0] = new Option('Overview', 'http://www.neopets.com/shenkuu/index.phtml');
				options[1] = new Option('Remarkable Restoratives', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=102');
				options[2] = new Option('Fanciful Fauna', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=103');
				options[3] = new Option('Wonderous Weaponry', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=100');
				options[4] = new Option('Exotic Foods', 'http://www.neopets.com/objects.phtml?type=shop&obj_type=101');
				options[5] = new Option('Culinary Concoctions', 'http://www.neopets.com/shenkuu/cooking.phtml');
				options[6] = new Option('Kou-Jong', 'http://www.neopets.com/games/kou-jong.phtml');
			}
			break;
			
	}
		
}