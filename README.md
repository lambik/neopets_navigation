# README #

This is a webpage that you can open locally on your pc (or on a webserver of course) so you have a nice shortcut menu for often-used things like dailies.

### How do I get set up? ###

* Download or clone the repository
* Open index.html in any browser

### Contribution guidelines ###

* You can contribute by adding/removing links, if you want
* Just fork and request a pull

